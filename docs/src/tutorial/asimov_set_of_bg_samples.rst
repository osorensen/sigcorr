Asimov set of background samples
================================


Method
------

In this chapter we will describe one of the ways to estimate the trials factor
for a generic signal search. The content of this chapter illustrates
the technical implementation of the method described in the work published
by V. Ananyev and A. L. Read [1]_.

The method operates on significance curves (or surfaces in 2D) that one can
calculate by combining background-only and signal+background likelihood scans
into likelihood ratios, and then using Wilks' theorem to translate this into
a significance. When there is only 1 extra degree of freedom in the signal
model, that has a fixed value under the background hypothesis, then the distribution
of the test statistic, in the asymptotic regime, follows a chi-square distribution with 1 degree of freedom (:math:`q \sim \chi^2`).

The sign of the significance can be inferred from the sign of the fitted parameter. In the case
described in the paper [1]_, this is the signal strength parameter :math:`\mu` (that is 0 under the background hypothesis). The statistical significance is therefore :math:`Z = \sqrt{q} \cdot \mathrm{sign}{\hat{\mu}}` [2]_.

The first important idea behind the method is that the significance curve (or surface) :math:`Z_i` can be viewed as
a Gaussian Process (GP) with mean :math:`\mu_i = 0` and standard deviation :math:`\Sigma_{ii} = 1`.

The challenge is to determine the values of the off-diagonal elements of the covariance matrix :math:`\Sigma_{ij}` of such a GP.
A modestly-sized set of Asimov background samples is suggested in the paper [1]_ to construct a representative set of artificial
MC realizations that incorporate enough information to estimate the full covariance matrix :math:`\Sigma_{ij}`.

*(Step 1. Building the set of Asimov background samples)* The set of Asimov background samples is a set of background samples with carefully chosen fluctuations.
Assuming the observable is binned, for each bin a :math:`1\sigma` upwards fluctuation is introduced,
while the rest of the bins follow the smooth Asimov background shape, as defined
in the original "Asimov paper" [2]_. The size of the Asimov set is given by the number of bins and is typically also
approximately the size of the scanning grid, i.e., the number of signal locations we test.

*(Step 2/3. Fitting samples. Estimating the covariance)* After doing the likelihood scans for
each Asimov background sample we end up with significance curves that we
then use to estimate the covariance matrix. We impose the assumption that the covariance is filled with
ones on the diagonal by replacing the covariance with the correlation matrix. We do this assuming the *data* in the bins of
the grid are uncorrelated.

*(Step 4. Sample GP to estimate the global p-value)* When we've determined the covariance matrix,
we randomly sample the GP many times. Each sample represents
a significance curve, i.e., a possible outcome of a complete likelihood-ratio scan of a random background-only MC experiment. Then we count the fraction of the sampled curves that exceed of some specified local significance threshold,
which gives us an estimate of the global p-value.

*(Step 5. Compute the trials factor)* Finally, the trials factor is a ratio of the global p-value
to the local p-value that we compute from the local significance and the estimated global significance.

In the sections below we will describe how to implement each step of the method with |project|. We will use the
``HyyTutorial`` model as an example. It is a model inspired by
a Higgs boson search in a diphoton decay spectrum. We introduced the model and described its parameters and the grid in
the previous chapters of the tutorial (:doc:`/tutorial/model_definition` and :doc:`/tutorial/grids`).
If you want a quick start, just copy :src:`sigcorr/models/hyy.py` into ``hyy_tutorial.py`` in the same place in your local repo to be aligned.


Step 1. Building the Asimov set of background samples
------------------------------------------------------
Following the definition above, we generate the background template and introduce a :math:`1\sigma` fluctuation
for each bin. For example, the ``HyyTutorial`` model
contains 61 points in the sampling grid and, consequently, 61 background samples.

The set of Asimov
samples differs from the brute force model only in the way background samples are produced.
In other words, :py:meth:`~sigcorr.models.model.AbstractModel.get_bg_samples` is the only method that requires modification to transition from the brute force approach to our Asimov approach, while the rest of the model
methods like model parameters, likelihoods and their derivatives stay unchanged.

In real life, however, it might happen that you don't have a brute force model implemented,
but still want to generate / fit an Asimov set of background samples to study the trials factor.
In this case you can follow the tutorial for :doc:`/tutorial/model_definition` and keep in
mind the special way of sampling the background samples for Asimov.

As a shortcut, we will use inheritance in Python to get a "copy" of the brute force
model (``HyyTutorial``) and then we will only override the :py:meth:`~sigcorr.models.model.AbstractModel.get_bg_samples` method.

Create a file in
:src:`sigcorr/models` named ``hyy_tutorial_asimov.py``, then the code below is enough to get
Asimov samples from the ``HyyTutorial`` brute force model:

.. code::

    from sigcorr.models.hyy_tutorial import HyyTutorial

    class HyyTutorialAsimov(HyyTutorial):
        def get_bg_samples(self, num_samples):
            expected_b = self.expected_b(self.B0, self.BG_XSCALE)  # <-- bg template
            return expected_b.reshape(1, -1) \
                 + self.BG_NOISE_STD * np.eye(expected_b.shape[0])  # <-- noise per bin


Step 2. Fitting Asimov samples
------------------------------

To fit the samples and produce an HDF5 file with Asimov sample likelihood scans we need to
run the :src:`sigcorr-run` script. From the tutorial :doc:`/tutorial/fitting` it follows that
:src:`sigcorr-run` requires a change below to learn about the new model:

.. code::

    from sigcorr.models.hyy_tutorial_asimov import HyyTutorialAsimov

    MODELS = [..., HyyTutorialAsimov]

.. WARNING::
    Remember to ``source env.sh`` before running the fits.

Let's run the Fitter (the number of samples doesn't matter - it is predefined by the Asimov model):

.. code:: bash

    sigcorr-run -g grids/hyy-common.dat -o tmp/hyy_tutorial.h5 -n 0 HyyTutorialAsimov

You can find an example of a resulting file in :toy_data:`Asimov1D/`.


Visualizing Asimov significance curves
--------------------------------------

Similarly to :ref:`tutorial/visualization_1d:Test statistic and significance` we start by reading the data:

.. code::

    import h5py

    with h5py.File("tmp/hyy_tutorial.h5", "r") as fin:
        fields = ["b_loglikes", "sb_loglikes", "sb_params"]
        bf_res = {k: fin[k][:61, ...] for k in fields}
        xs = fin["scan_xs"][...].ravel()


Example of plotting several significance curves:


.. code::

    from sigcorr.tools.stats.utils import get_delta_sigs

    ...
    sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

.. plot:: src/asimov_sigcurve.py


Step 3. Asimov covariance matrix
--------------------------------


We will use :py:class:`~sigcorr.tools.stats.batch_stats.BatchStats2` for the covariance calculation as
described in :ref:`tutorial/visualization_1d:Covariance matrix`. While computing the covariance,
we use known properties of the significance curves. We know that the local significance follows the standard normal distribution with mean 0
and variance 1. By overriding the ``x`` with 0 we are forcing the former condition, and by computing the correlation instead
of the covariance we impose the latter.


.. code::

    from sigcorr.tools.stats.batch_stats import BatchStats2

    ...
    num_samples = sigs.shape[0]
    sample_dim = sigs.shape[1]
    bs = BatchStats2(num_samples, sample_dim)
    bs.push(sigs)
    cov = bs.get_corr(override_x=np.array([0.]))

The resulting Asimov covariance can be compared to the covariance calculated from brute force toys :ref:`tutorial/visualization_1d:Covariance matrix`:

.. plot:: src/asimov_covmat.py


Step 3 (alternative). Estimation of the covariance matrix by linear expansion around the best background-only fit.
------------------------------------------------------------------------------------------------------------------


If the model implements :py:class:`~sigcorr.models.model.LinearApproximable` protocol,
one can use :py:func:`~sigcorr.linear_approx_sigcov.get_linear_approx_sigcov_for_model`
to estimate the covariance matrix analytically.

For this, one background-only fit to the data is required.
For the code snippet below we assume ``bf_res`` contains fit results to the non-Asimov (i.e. raw or MC) data samples.

.. code::

    from sigcorr.linear_approx_sigcov import get_linear_approx_sigcov_for_model

    ...
    b_params = bf_res["b_params"][0, np.newaxis]
    cov = get_linear_approx_sigcov_for_model(model, xs, b_params)[0]


Although it is not required to have many fits, in this example we assume that we have several,
to maintain the same structure of the variable ``b_params`` as we introduced previously in the text.
We, however, pick the fitted parameters for one sample, as it is enough to build the prediction.

Notice the way we use ``np.newaxis`` and then extract the first fit result with ``get_linear_sigcov_for_model(...)[0]``.
The reason for such complexity is that
:py:func:`~sigcorr.linear_approx_sigcov.get_linear_approx_sigcov_for_model` is vectorized
and can efficiently estimate the covariance for a set of fitted parameters, for example extracted from a set of MC fits.

Since constructing the covariance in this way is so easy, in addition to the ``HyyTutorial`` model, we also estimate
the covariance for the background template model introduced by Gross and Vitells [3]_
and later studied by Ananiev and Read [1]_:

.. plot:: src/linear_approx_sigcov.py


Step 4/5. Trials factor from GP toys
------------------------------------


We will use :doc:`/tutorial/batch_multiprocessing` to sample GP toys from the Asimov covariance
with :py:func:`~sigcorr.mapreduce.gp.gp_batch_mapreduce`. Then we apply the
:py:class:`~sigcorr.mapreduce.map_reducers.OverflowsCalc` calculator to identify whether each
curve exceeds the specified thresholds of the local significance (``local_sig_grid`` in the code below). The resulting array of over-threshold signatures for each curve and local significance value is passed to the :py:class:`~sigcorr.mapreduce.map_reducers.BatchStats1Reduce` reducer that will compute the mean value for each significance threshold, and, therefore, will estimate the fractions of significance curves that exceed the thresholds, i.e., the global p-values:

.. code::

    import numpy as np
    from sigcorr.mapreduce.gp import gp_batch_mapreduce
    from sigcorr.mapreduce.map_reducers import OverflowsCalc
    from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
    from sigcorr.tools.utils import get_last_from_iter

    ...
    local_sig_grid = np.arange(0, 3.05, 0.1)  # i.e. significances between 0 and 3 with step 0.1

    p_global_bs, _ = get_last_from_iter(
        gp_batch_mapreduce(cov,
                           1_000_000,  # num samples
                           1000,  # batch size
                           xs.shape,
                           OverflowsCalc(local_sig_grid),
                           BatchStats1Reduce())
    )
    p_global = p_global_bs.get_mean()
    p_global_err = p_global_bs.get_stat_err()

We then compute the local p-values from the local significances and by definition of the trials factor (:math:`TF = \frac{p_{global}}{p_{local}}`) compute the Asimov estimate for the trials factor:

.. code::

    from sigcorr.tools.stats.utils import sig2pval

    ...
    local_p = sig2pval(local_sig_grid)
    tf_gp = p_global/local_p
    tf_gp_err = p_global_err/local_p

.. plot:: src/asimov_tf.py


Gross and Vitells upper bound from up-crossings
-----------------------------------------------


As described in :ref:`tutorial/visualization_1d:Gross and Vitells upper bound from up-crossings`, there are two ways to set the Gross and Vitells upper bound, that are equivalent to two ways of estimating
the Euler characteristic of the significance curve at some significance threshold:

#. By counting the up-crossings manually from the brute force toys at some significance level,
   estimating the average Euler characteristic at this level and then propagating it to other local significances.

   In this chapter, however, we don't work with brute force toys, and 61 fitted Asimov curves is not enough
   to estimate the average number of up-crossings precisely. Therefore, for presentation purposes, we will
   count the average number of up-crossing from the GP toys and compare the :math:`TF` to the one computed
   from the up-crossings density estimated analytically.

    .. code::

        from sigcorr.tools.stats.gp.euler_number import avg_gp_euler_number
        from sigcorr.tools.stats.gp.euler_number import GPEulerNumberPropagator

        from sigcorr.mapreduce.gp import gp_batch_mapreduce
        from sigcorr.mapreduce.map_reducers import UpcrossCalc
        from sigcorr.mapreduce.map_reducers import BatchStats1Reduce
        from sigcorr.tools.utils import get_last_from_iter

        ...
        ref_sigs = np.array([0.7])

        pipeline = gp_batch_mapreduce(cov,
                                      1_000_000,  # num samples
                                      1000,  # batch size
                                      xs.shape,
                                      UpcrossCalc(ref_sigs[0]),
                                      BatchStats1Reduce())
        avg_upcross_bs, _ = get_last_from_iter(pipeline)
        avg_euler_num = avg_gp_euler_number(ref_sigs, avg_upcross_bs.get_mean())
        p_global_upper = GPEulerNumberPropagator(ref_sigs, avg_euler_nums).calc(local_sig_grid)

        tf_upper_bound_from_samples = p_global_upper/local_p

#. Estimating the up-crossings from the GP covariance at all needed levels from the analytical
   expression for the up-crossings density of the GP, then computing the Euler characteristic for each
   of them:

    .. code::

        from sigcorr.tools.stats.gp.upcross import gp_upcross_at_level
        from sigcorr.tools.stats.gp.euler_number import avg_gp_euler_number

        ...
        avg_upcross = np.array([gp_upcross_at_level(xs, cov, sig) for sig in local_sig_grid])
        avg_euler_numbers = avg_gp_euler_number(local_sig_grid, avg_upcross)
        tf_upper_bound_from_cov = avg_euler_numbers

Ideally we expect these two approaches to match precisely, however, imperfections may be present due to numerical precision. In the plot below you can see the various estimates of :math:`TF`:

.. plot:: src/asimov_gv_upper.py

References
----------

.. [1] V. Ananyev and A. L. Read, "Gaussian Process-based calculation of look-elsewhere trials factor,"
       JINST 18 (2023) P05041, https://doi.org/10.1088/1748-0221/18/05/P05041
.. [2] G. Cowan and K. Cranmer and E. Gross et al. "Asymptotic formulae for likelihood-based tests of new physics,"
       Eur. Phys. J. C 71, 1554 (2011), https://doi.org/10.1140/epjc/s10052-011-1554-0
.. [3] Gross, E. and Vitells, O. "Trial factors for the look elsewhere effect in high energy physics,"
       Eur. Phys. J. C 70, 525–530 (2010). https://doi.org/10.1140/epjc/s10052-010-1470-8
