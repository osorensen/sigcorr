MapReduce building blocks (MapReducers)
=======================================

.. automodule:: sigcorr.mapreduce.map_reducers
   :members:
