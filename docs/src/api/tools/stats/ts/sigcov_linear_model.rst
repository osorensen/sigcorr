Significance covariance matrix for Linear model with Gaussian errors
====================================================================

.. automodule:: sigcorr.tools.stats.ts.sigcov_linear_model
   :undoc-members:
   :members: get_sigcov_for_linear_model
