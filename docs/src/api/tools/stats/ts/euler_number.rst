Euler number
============

.. automodule:: sigcorr.tools.stats.ts.euler_number
   :undoc-members:
   :members: avg_ts_euler_number, avg_ts_euler_number_propagated
