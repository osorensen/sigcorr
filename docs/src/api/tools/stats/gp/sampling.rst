Sampling
========

.. automodule:: sigcorr.tools.stats.gp.sampling
   :undoc-members:
   :members:
   :exclude-members: gibbs_kernel
