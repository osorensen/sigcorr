import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs
from sigcorr.tools.stats.batch_stats import BatchStats2


bf_res = None
with h5py.File("../data/Asimov1D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:61, ...] for k in fields}
    xs = fin["scan_xs"][...].ravel()


sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

num_samples = sigs.shape[0]
sample_dim = sigs.shape[1]
bs = BatchStats2(num_samples, sample_dim)
bs.push(sigs)
cov = bs.get_corr(override_x=np.array([0.]))
cov_err = bs.get_corr_stat_err(override_x=np.array([0.]))

mesh = plt.pcolormesh(xs, xs, cov)
plt.xlabel("m, GeV")
plt.ylabel("m, GeV")
plt.gca().invert_yaxis()
plt.title("Asimov covariance matrix")
plt.colorbar(mesh, ax=plt.gca())


if __name__ == "__main__":
    plt.show()
