import h5py
import matplotlib.pyplot as plt
from sigcorr.tools.stats.utils import get_delta_sigs


bf_res = None
with h5py.File("../data/Asimov2D/hyy_tutorial.h5", "r") as fin:
    fields = ["b_loglikes", "sb_loglikes", "sb_params"]
    bf_res = {k: fin[k][:61, ...] for k in fields}
    xs = fin["scan_xs"][...]

sigs = get_delta_sigs(bf_res["b_loglikes"], bf_res["sb_loglikes"], bf_res["sb_params"][..., 0])

masses = xs[:, 0, 0]
widths = xs[0, :, 1]

plt.title("Significance surface")
mesh = plt.pcolormesh(widths, masses, sigs[35])
plt.xlabel("width, GeV")
plt.ylabel("m, GeV")
plt.colorbar(mesh, ax=plt.gca())


if __name__ == "__main__":
    plt.show()
