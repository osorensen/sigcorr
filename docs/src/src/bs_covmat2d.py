import h5py
import numpy as np
import matplotlib.pyplot as plt
from sigcorr.mapreduce.file import h5_batch_mapreduce
from sigcorr.mapreduce.map_reducers import ChainCalc
from sigcorr.mapreduce.map_reducers import SigsCalc
from sigcorr.mapreduce.map_reducers import MathCalc
from sigcorr.mapreduce.map_reducers import BatchStats2Reduce
from sigcorr.tools.utils import get_last_from_iter


with h5py.File("../data/Asimov2D/hyy_tutorial.h5", "r") as fin:
    xs = fin["scan_xs"][...]


pipeline = h5_batch_mapreduce("../data/Asimov2D/hyy_tutorial.h5", ["b_loglikes", "sb_loglikes", "sb_params"],
                              10,  # batch size
                              ChainCalc([SigsCalc(), MathCalc(lambda b: b.reshape(b.shape[0], -1))]),
                              BatchStats2Reduce())
bs, _ = get_last_from_iter(pipeline)
cov = bs.get_corr(override_x=np.array([0.]))


masses = xs[:, 0, 0]
masses_dim = masses.shape[0]

widths = xs[0, :, 1]
widths_dim = widths.shape[0]

# downsample for better plotting performance and visual experience
cov4d = cov.reshape(masses_dim, widths_dim, masses_dim, widths_dim)
cov4d_downsample = cov4d[::3, ::3, ::3, ::3]
downsampled_dim = int(np.product(cov4d_downsample.shape[:2]))
cov_downsample = cov4d_downsample.reshape(downsampled_dim, downsampled_dim)

lin_xs = np.arange(0, downsampled_dim, 1)

mesh = plt.pcolormesh(lin_xs, lin_xs, cov_downsample)
plt.xlabel(r'$M_i$')
plt.ylabel(r'$M_j$')
plt.title("Covariance matrix")
plt.colorbar(mesh, ax=plt.gca())


if __name__ == "__main__":
    plt.show()
