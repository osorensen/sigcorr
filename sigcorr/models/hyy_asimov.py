import numpy as np
from sigcorr.models.hyy import Hyy


class HyyAsimovCommon(Hyy):
    _NSIGMA = None

    def get_bg_samples(self, num_samples):
        expected_b = self.expected_b(self.B0, self.BG_XSCALE)
        return expected_b.reshape(1, -1) \
             + self._NSIGMA*self.BG_NOISE_STD * np.eye(expected_b.shape[0])


class HyyAsimov1Sigma(HyyAsimovCommon):
    _NSIGMA = 1


class HyyAsimov(HyyAsimov1Sigma):
    pass


class HyyAsimov2Sigma(HyyAsimovCommon):
    _NSIGMA = 2


class HyyAsimov3Sigma(HyyAsimovCommon):
    _NSIGMA = 3


class HyyAsimov4Sigma(HyyAsimovCommon):
    _NSIGMA = 1


class HyyAsimov1SigmaNeg(HyyAsimovCommon):
    _NSIGMA = -1


class HyyAsimov2SigmaNeg(HyyAsimovCommon):
    _NSIGMA = -2


class HyyAsimov3SigmaNeg(HyyAsimovCommon):
    _NSIGMA = -3


class HyyAsimov4SigmaNeg(HyyAsimovCommon):
    _NSIGMA = -4
