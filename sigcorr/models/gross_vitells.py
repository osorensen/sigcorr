import numpy as np
import jax.numpy as jnp
import scipy as sp
import jax.scipy as jsp
from scipy import stats  # pylint: disable=unused-import # noqa: F401
from scipy import optimize  # pylint: disable=unused-import # noqa: F401
from sigcorr.models.model import (
    AbstractModel,
    LinearApproximable,
)
from sigcorr.tools.utils import jax_jit_method


class GrossVitells(AbstractModel, LinearApproximable):
    PARAMS = ["B0", "BG_SCALE", "SIG_STD", "SIG_STD_SLOPE"]

    def __init__(self, xs):
        self.B0 = 2000
        self.BG_SCALE = 40
        self.SIG_STD = 2.5
        self.SIG_STD_SLOPE = 50
        self.xs = xs

    def init(self):
        self.xs = self.xs.ravel()
        self.sb_guess = np.array([30., 1000.])
        self.b_guess = np.array([1000.])
        self._bg_shape = self._get_bg_shape()
        return self

    def _get_bg_shape(self):
        res = sp.stats.rayleigh(scale=self.BG_SCALE).pdf(self.xs)
        return res/res.sum()

    def get_bg_samples(self, num_samples):
        return sp.stats.poisson(self.expected_b(self.B0)) \
                       .rvs((num_samples, self.xs.shape[0])).astype("float64")

    @jax_jit_method
    def sb_minus_loglike(self, p, sample, signal_x):
        lam = self.expected_sb(signal_x, p[:1], p[1:])
        res = jnp.sum(sample*jnp.log(lam + 0j))
        return -jnp.real(res) + jnp.imag(res)*1000 + lam.sum()

    @jax_jit_method
    def sb_minus_loglike_grad(self, p, sample, signal_x):
        signal_shape = self.expected_signal(signal_x, 1)
        lam = self.expected_sb(signal_x, p[:1], p[1:])
        return jnp.array([jnp.sum(-sample*signal_shape/lam + signal_shape),
                          jnp.sum(-sample*self._bg_shape/lam + self._bg_shape)])

    @jax_jit_method
    def sb_minus_loglike_hess(self, p, sample, signal_x):
        S = self.expected_signal(signal_x, 1)
        lam_sqr = self.expected_sb(signal_x, p[:1], p[1:])**2
        dp0p0 = jnp.sum(sample*S**2/lam_sqr)
        dp0p1 = jnp.sum(sample*self._bg_shape*S/lam_sqr)
        dp1p1 = jnp.sum(sample*self._bg_shape**2/lam_sqr)
        return jnp.array([[dp0p0, dp0p1],
                          [dp0p1, dp1p1]])

    @jax_jit_method
    def expected_sb(self, sig_x, sig_params, bg_params):
        return self.expected_signal(sig_x, *sig_params) + self.expected_b(*bg_params)

    @jax_jit_method
    def expected_signal(self, sig_x, n_sig):
        return n_sig*jsp.stats.norm.pdf(self.xs, loc=sig_x, scale=self.SIG_STD*(1+sig_x/self.SIG_STD_SLOPE))

    @jax_jit_method
    def b_minus_loglike(self, p, sample):
        lam = self.expected_b(*p)
        res = jnp.sum(sample*jnp.log(lam + 0j))
        return -jnp.real(res) + jnp.imag(res)*1000 + lam.sum()

    @jax_jit_method
    def b_minus_loglike_grad(self, p, sample):
        lam = self.expected_b(*p)
        return jnp.array([jnp.sum(-sample*self._bg_shape/lam + self._bg_shape)])

    @jax_jit_method
    def b_minus_loglike_hess(self, p, sample):
        lam = self.expected_b(*p)
        return jnp.array([[jnp.sum(sample*(self._bg_shape/lam)**2)]])

    @jax_jit_method
    def expected_b(self, n_bg):
        return n_bg*self._bg_shape

    @jax_jit_method
    def expected_b_grad(self, n_bg):
        new_shape = n_bg.shape + (1, ) + self._bg_shape.shape
        # num_samples x bg_params x xs
        return jnp.broadcast_to(self._bg_shape, new_shape)
