import numpy as np
import jax.numpy as jnp
import scipy as sp
import jax.scipy as jsp
from scipy import stats  # pylint: disable=unused-import # noqa: F401
from scipy import optimize  # pylint: disable=unused-import # noqa: F401
from sigcorr.models.model import (
    AbstractModel,
    LinearApproximable,
)
from sigcorr.tools.utils import jax_jit_method


class Hyy(AbstractModel, LinearApproximable):
    PARAMS = ["B0", "BG_XSCALE", "BG_NOISE_STD", "SIG_STD"]

    def __init__(self, xs):
        self.B0 = 10
        self.BG_XSCALE = 0.033
        self.BG_NOISE_STD = 0.3
        self.SIG_STD = 5.
        self.xs = xs

    def init(self):
        self.xs = self.xs.ravel()
        self.b_guess = np.array([self.B0, self.BG_XSCALE])*1.2
        self.sb_guess = np.array([0.1, self.B0, self.BG_XSCALE])*1.2
        return self

    def get_bg_samples(self, num_samples):
        return self.expected_b(self.B0, self.BG_XSCALE) + \
               sp.stats.norm(scale=self.BG_NOISE_STD).rvs((num_samples, self.xs.shape[0]))

    @jax_jit_method
    def sb_minus_loglike(self, p, sample, signal_x):
        return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                              loc=self.expected_sb(signal_x, p[:1], p[1:]),
                                              scale=self.BG_NOISE_STD))

    @jax_jit_method
    def sb_minus_loglike_grad(self, p, sample, signal_x):
        S_i = self.expected_signal(signal_x, 1)
        B_i = self.expected_b(1, p[2])
        mu_i = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu_i - sample
        dp0 = jnp.sum(deviation*S_i)
        dp1 = jnp.sum(deviation*B_i)
        dp2 = -jnp.sum(deviation*p[1]*B_i*(self.xs - 100))
        return jnp.hstack([dp0, dp1, dp2])/self.BG_NOISE_STD**2

    @jax_jit_method
    def sb_minus_loglike_hess(self, p, sample, signal_x):
        S_shape = self.expected_signal(signal_x, 1)
        B_shape = self.expected_b(1, p[2])
        B = p[1]*self.expected_b(1, p[2])
        mu = self.expected_sb(signal_x, p[:1], p[1:])
        deviation = mu - sample
        B_exp = (self.xs - 100)
        dp0p0 = jnp.sum(S_shape**2)
        dp0p1 = jnp.sum(S_shape*B_shape)
        dp0p2 = -jnp.sum(S_shape*B_exp*B)
        dp1p1 = jnp.sum(B_shape**2)
        dp1p2 = jnp.sum(-B_exp*(deviation + B)*B_shape)
        dp2p2 = jnp.sum((deviation + B)*B_exp**2*B)
        return jnp.array([[dp0p0, dp0p1, dp0p2],
                          [dp0p1, dp1p1, dp1p2],
                          [dp0p2, dp1p2, dp2p2]])/self.BG_NOISE_STD**2

    @jax_jit_method
    def expected_sb(self, sig_x, sig_params, bg_params):
        return self.expected_signal(sig_x, *sig_params) + self.expected_b(*bg_params)

    @jax_jit_method
    def expected_signal(self, signal_x, n_sig):
        return n_sig*jsp.stats.norm.pdf(self.xs, loc=signal_x, scale=self.SIG_STD)

    @jax_jit_method
    def b_minus_loglike(self, p, sample):
        return -jnp.sum(jsp.stats.norm.logpdf(sample,
                                              loc=self.expected_b(*p),
                                              scale=self.BG_NOISE_STD))

    @jax_jit_method
    def b_minus_loglike_grad(self, p, sample):
        B_i = self.expected_b(1, p[1])
        mu_i = self.expected_b(*p)
        deviation = mu_i - sample
        dp0 = jnp.sum(deviation*B_i)
        dp1 = -jnp.sum(deviation*mu_i*(self.xs - 100))
        return jnp.hstack([dp0, dp1])/self.BG_NOISE_STD**2

    @jax_jit_method
    def b_minus_loglike_hess(self, p, sample):
        B_shape = self.expected_b(1, p[1])
        mu = p[0]*B_shape
        deviation = mu - sample
        B_exp = self.xs - 100
        dp0p0 = jnp.sum(B_shape**2)
        dp0p1 = jnp.sum(-B_exp*(deviation + mu)*B_shape)
        dp1p1 = jnp.sum((deviation + mu)*B_exp**2*mu)
        return jnp.array([[dp0p0, dp0p1],
                          [dp0p1, dp1p1]])/self.BG_NOISE_STD**2

    @jax_jit_method
    def expected_b(self, n_bg, xscale):
        return n_bg*jnp.exp(-(self.xs-100)*xscale)

    @jax_jit_method
    def expected_b_grad(self, n_bg, xscale):
        p_dims = max(n_bg.ndim, xscale.ndim)
        xs_ext = jnp.expand_dims(self.xs, tuple(range(p_dims+1)))
        n_bg_ext = jnp.expand_dims(n_bg, (-1, -2))
        xscale_ext = jnp.expand_dims(xscale, (-1, -2))
        exponent = (xs_ext-100)*xscale_ext

        # num_samples x bg_params x xs
        return jnp.concatenate([
            jnp.exp(-exponent),
            -n_bg_ext*exponent*jnp.exp(-exponent)
        ], axis=1)
